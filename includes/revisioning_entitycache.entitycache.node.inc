<?php

/**
 * @file
 * Special Revisioning Node entity controller with persistent cache.
 */

/**
 * Special Revisioning Node entity controller with persistent cache.
 */
class RevisioningEntityCacheNodeController extends NodeController {

  /**
   * {@inheritdoc}
   */
  public function resetCache(array $ids = NULL) {
    RevisioningEntityCacheControllerHelper::resetEntityCache($this, $ids);
    parent::resetCache($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function load($ids = array(), $conditions = array()) {
    return RevisioningEntityCacheControllerHelper::entityCacheLoad($this, $ids, $conditions);
  }
}
