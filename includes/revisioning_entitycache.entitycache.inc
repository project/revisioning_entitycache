<?php

/**
 * @file
 * Special Entity Cache Controller helper with revisioning support.
 */

/**
 * Special Entity Cache Controller helper with revisioning support.
 */
class RevisioningEntityCacheControllerHelper extends EntityCacheControllerHelper {

  protected static $entityRevisionCache;

  /**
   * {@inheritdoc}
   */
  public static function resetEntityCache($controller, array $ids = NULL) {
    $entity_type = $controller->entityType;
    $bin = 'cache_entity_' . $entity_type;

    // Handle special case when entity_load() clears the whole cache when
    // reset is passed in.
    if (empty($ids)) {
      $trace = debug_backtrace(FALSE, 3);
      array_shift($trace);
      array_shift($trace);

      // As we do not want to clear all of the persistently cached entities,
      // we determine the arguments to entity_load via the backtrace.
      if (isset($trace[0]['function']) && $trace[0]['function'] == 'entity_load' && isset($trace[0]['args'][1])) {
        $ids = $trace[0]['args'][1];
        // But clear the whole static cache - like usual.
        unset(static::$entityRevisionCache[$entity_type]);
      }
    }

    parent::resetEntityCache($controller, $ids);

    // Reset the revisions in the entity cache as well.
    if (!empty($ids)) {
      foreach ($ids as $entity_id) {
        unset(static::$entityRevisionCache[$entity_type][$entity_id]);
        cache_clear_all($entity_id . ':', $bin, TRUE);
      }
    }
    else {
      unset(static::$entityRevisionCache[$entity_type]);
      $e = new \Exception();
      watchdog('revisioning_entitycache', 'The whole entitycache for the @entity_type entity type was cleared. Backtrace: @bt', array(
        '@entity_type' => $entity_type,
        '@bt' => $e->getTraceAsString()
      ), WATCHDOG_WARNING);
    }
  }


  /**
   * {@inheritdoc}
   */
  public static function entityCacheLoad($controller, $ids = array(), $conditions = array()) {
    // Early return if the entity type is not 'node'.
    if ($controller->entityType != 'node') {
      return parent::entityCacheLoad($controller, $ids, $conditions);
    }

    // Early return if we don't want to load a revision or there is more conditions or there is more than one id.
    if (!$controller->revisionKey || !isset($conditions[$controller->revisionKey]) || count($conditions) != 1 || count($ids) != 1) {
      return parent::entityCacheLoad($controller, $ids, $conditions);
    }

    // If we got a revision we only ever have one entity_id.
    $revision_id = $conditions[$controller->revisionKey];
    $entity_id = reset($ids);
    $entity_type = $controller->entityType;

    // If the revision is the current vid, then just load the enttiy without a revision.
    $current_revision_id = revisioning_get_current_node_revision_id($entity_id);

    if ($revision_id == $current_revision_id) {
      unset($conditions[$controller->revisionKey]);
      return parent::entityCacheLoad($controller, $ids, $conditions);
    }

    // If the revision is not the latest vid, then bail out.
    $latest_revision_id = revisioning_get_latest_revision_id($entity_id);

    if ($revision_id != $latest_revision_id) {
      return parent::entityCacheLoad($controller, $ids, $conditions);
    }

    // See if we have the node for the $revision_id statically cached.
    $cached_entities = array();

    if (isset(static::$entityRevisionCache[$entity_type][$entity_id][$revision_id])) {
      $cached_entities[$entity_id] = static::$entityRevisionCache[$entity_type][$entity_id][$revision_id];
      return $cached_entities;
    }

    // See if we have the node in the persistent cache.
    $cid = $entity_id . ':' . $revision_id;
    $bin = 'cache_entity_' . $entity_type;

    $cache = cache_get($cid, $bin);
    if ($cache) {
      $cached_entities[$entity_id] = $cache->data;
      static::entityCacheAttachLoad($controller, $cached_entities);

      static::$entityRevisionCache[$entity_type][$entity_id][$revision_id] = clone $cached_entities[$entity_id];
      return $cached_entities;
    }

    // Fetch it from the database.
    $entities = parent::entityCacheLoad($controller, $ids, $conditions);

    // Return if there is no result.
    if (!isset($entities[$entity_id])) {
      return $entities;
    }

    // Cache the entity permanently per revision.
    static::$entityRevisionCache[$entity_type][$entity_id][$revision_id] = $entities[$entity_id];
    cache_set($cid, $entities[$entity_id], $bin);

    return $entities;
  }
}
